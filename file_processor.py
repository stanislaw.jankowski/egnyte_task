import logging
import urllib.request
from http import HTTPStatus
from logging.config import fileConfig
from urllib.error import URLError


class FileProcessor:
    """Downloads a text file and searches for a row with smallest difference between values in 2 desirable columns."""

    def __init__(self, url, validate_row, prepare_row):
        """Initializes FileProcessor.

        Args:
            url (str): Url indicating the file to be downloaded and processed.
            validate_row (function): Function to validate single data row.
                Returns: True for valid row, False for invalid row.
            prepare_row (function): Function to preprocess a single data row.
                Returns: a tuple (name, val1, val2), where: name is the name of row;
                    val1, val2 - 2 desired int values to count a difference between them.
        """

        self.__url = url
        self.__validate_row = validate_row
        self.__prepare_row = prepare_row

    def process_file(self):
        """Process file."""
        try:
            raw_data = self.__fetch_data()
            data = self.__extract_desirable_data(raw_data)
            return self.__get_smallest_difference(data)
        except URLError as e:
            raise ConnectionError("Can not fetch data from {}.".format(self.__url)).with_traceback(e.__traceback__)
        except (IndexError, TypeError, AttributeError) as e:
            raise AttributeError(
                "Probably file url or functions passed to the class constructor are invalid."
            ).with_traceback(e.__traceback__)

    def __fetch_data(self):
        """Downloads a file from given url.

        Returns:
            A list with all lines read from file.

        """
        with urllib.request.urlopen(self.__url) as response:
            if response.code == HTTPStatus.OK:
                return response.read().decode("utf-8").splitlines()
            else:
                raise URLError("Response code {}".format(response.code))

    def __extract_desirable_data(self, data):
        """Validates data lines and prepares a list of tuples needed for further processing.

        Returns:
            A list of tuples: (name, val1, val2).
        """
        return [self.__prepare_row(row.split()) for row in data if self.__validate_row(row.split())]

    @staticmethod
    def __get_smallest_difference(data):
        """Counts the smallest difference between values in data rows.

        Args:
            data: (list): A list of tuples: (name, val1, val2).

        Returns:
            A name of tuple with smallest difference between val1 and val2.
        """
        return min(data, key=lambda x: abs(x[1] - x[2]))[0]


if __name__ == "__main__":
    fileConfig('logging_config.ini')
    logger = logging.getLogger()
    weather_processor = FileProcessor("http://codekata.com/data/04/weather.dat",
                                      lambda row: False if row is None or len(row) == 0 else row[0].isdigit(),
                                      lambda row: (row[0], int(row[1].replace('*', '')), int(row[2].replace('*', ''))))

    football_processor = FileProcessor("http://codekata.com/data/04/football.dat",
                                       lambda row: False if row is None or len(row) == 1 else row[6].isdigit(),
                                       lambda row: (row[1], int(row[6]), int(row[8])))
    try:
        print(weather_processor.process_file())
        print(football_processor.process_file())
    except Exception as e:
        logger.error("{}".format(e), exc_info=True)
        print("ERROR: {}\nCheck log file for details.".format(e))
