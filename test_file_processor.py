from http import HTTPStatus
from urllib.error import URLError

import pytest

import tests_data
from file_processor import FileProcessor

football_url = "http://codekata.com/data/04/football.dat"
football_validate_row = lambda row: False if row is None or len(row) == 1 else row[6].isdigit()
football_prepare_row = lambda row: (row[1], int(row[6]), int(row[8]))
football_validate_row_broken_index = lambda row: False if row is None or len(row) == 1 else row[66].isdigit()
football_prepare_row_broken_index = lambda row: (row[1], int(row[66]), int(row[8]))
football_result = 'Aston_Villa'

weather_url = "http://codekata.com/data/04/weather.dat"
weather_validate_row = lambda row: False if row is None or len(row) == 0 else row[0].isdigit()
weather_prepare_row = lambda row: (row[0], int(row[1].replace('*', '')), int(row[2].replace('*', '')))
weather_validate_row_broken_index = lambda row: False if row is None or len(row) == 0 else row[60].isdigit()
weather_prepare_row_broken_index = lambda row: (row[0], int(row[1].replace('*', '')), int(row[62].replace('*', '')))
weather_result = '14'

broken_lambda_int = 3


class MockResponse:
    """Mocks urllib response"""

    def __init__(self, url, code):
        self.code = code
        self.url = url
        self.reason = ""

    def __enter__(self):
        return self

    def __exit__(self, a, b, c):
        pass

    def read(self):
        if self.url == football_url:
            return tests_data.football_binary_data
        elif self.url == weather_url:
            return tests_data.weather_binary_data


def urllib_request_urlopen(url):
    return MockResponse(url, HTTPStatus.OK)


def urllib_request_urlopen_code_internal_server_error(url):
    return MockResponse(url, HTTPStatus.INTERNAL_SERVER_ERROR)


def urllib_request_urlopen_attribute_error(url):
    raise AttributeError()


def urllib_request_urlopen_index_error(url):
    raise IndexError()


def urllib_request_urlopen_type_error(url):
    raise TypeError()


@pytest.fixture
def mock_urllib_request_urlopen(monkeypatch):
    monkeypatch.setattr("urllib.request.urlopen", urllib_request_urlopen)


@pytest.fixture(
    params=[urllib_request_urlopen_attribute_error,
            urllib_request_urlopen_index_error,
            urllib_request_urlopen_type_error])
def mock_urllib_request_urlopen_error(monkeypatch, request):
    monkeypatch.setattr("urllib.request.urlopen", request.param)


@pytest.fixture
def mock_urllib_request_urlopen_code_internal_server_error(monkeypatch):
    monkeypatch.setattr("urllib.request.urlopen", urllib_request_urlopen_code_internal_server_error)


@pytest.mark.parametrize(
    'url, validate_row, prepare_row, raw_data',
    [(football_url, football_validate_row, football_prepare_row, tests_data.football_raw_data),
     (weather_url, weather_validate_row, weather_prepare_row, tests_data.weather_raw_data)
     ])
def test___fetch_data(url, validate_row, prepare_row, raw_data, mock_urllib_request_urlopen):
    file_processor = FileProcessor(url, validate_row, prepare_row)
    raw = file_processor._FileProcessor__fetch_data()
    assert raw == raw_data


@pytest.mark.parametrize(
    'url, validate_row, prepare_row',
    [(football_url, football_validate_row, football_prepare_row),
     (weather_url, weather_validate_row, weather_prepare_row)
     ])
def test___fetch_data_return_code_internal_server_error(url, validate_row, prepare_row,
                                                        mock_urllib_request_urlopen_code_internal_server_error):
    file_processor = FileProcessor(url, validate_row, prepare_row)
    with pytest.raises(URLError):
        file_processor._FileProcessor__fetch_data()


@pytest.mark.parametrize(
    'url, validate_row, prepare_row, raw_data, desirable_data',
    [(football_url, football_validate_row, football_prepare_row, tests_data.football_raw_data,
      tests_data.football_desirable_data),
     (weather_url, weather_validate_row, weather_prepare_row, tests_data.weather_raw_data,
      tests_data.weather_desirable_data)
     ])
def test___extract_desirable_data(url, validate_row, prepare_row, raw_data, desirable_data):
    file_processor = FileProcessor(url, validate_row, prepare_row)
    data = file_processor._FileProcessor__extract_desirable_data(raw_data)
    assert data == desirable_data


@pytest.mark.parametrize(
    'url, validate_row, prepare_row, raw_data',
    [(football_url, broken_lambda_int, football_prepare_row, tests_data.football_raw_data),
     (weather_url, broken_lambda_int, weather_prepare_row, tests_data.weather_raw_data),
     (football_url, football_validate_row, broken_lambda_int, tests_data.football_raw_data),
     (weather_url, weather_validate_row, broken_lambda_int, tests_data.weather_raw_data)
     ])
def test___extract_desirable_data_broken_lambda_int(url, validate_row, prepare_row, raw_data):
    file_processor = FileProcessor(url, validate_row, prepare_row)
    with pytest.raises(TypeError):
        file_processor._FileProcessor__extract_desirable_data(raw_data)


@pytest.mark.parametrize(
    'url, validate_row, prepare_row, raw_data',
    [(football_url, football_validate_row_broken_index, football_prepare_row, tests_data.football_raw_data),
     (weather_url, weather_validate_row_broken_index, weather_prepare_row, tests_data.weather_raw_data),
     (football_url, football_validate_row, football_prepare_row_broken_index, tests_data.football_raw_data),
     (weather_url, weather_validate_row, weather_prepare_row_broken_index, tests_data.weather_raw_data)
     ])
def test___extract_desirable_data_lambda_index_out_of_range(url, validate_row, prepare_row, raw_data):
    file_processor = FileProcessor(url, validate_row, prepare_row)
    with pytest.raises(IndexError):
        file_processor._FileProcessor__extract_desirable_data(raw_data)


@pytest.mark.parametrize(
    'url, validate_row, prepare_row, desirable_data, expected_result',
    [(football_url, football_validate_row, football_prepare_row, tests_data.football_desirable_data, football_result),
     (weather_url, weather_validate_row, weather_prepare_row, tests_data.weather_desirable_data, weather_result)
     ])
def test___get_smallest_difference(url, validate_row, prepare_row, desirable_data, expected_result):
    file_processor = FileProcessor(url, validate_row, prepare_row)
    result = file_processor._FileProcessor__get_smallest_difference(desirable_data)
    assert result == expected_result


@pytest.mark.parametrize(
    'url, validate_row, prepare_row, expected_result',
    [(football_url, football_validate_row, football_prepare_row, football_result),
     (weather_url, weather_validate_row, weather_prepare_row, weather_result)
     ])
def test_process_file(url, validate_row, prepare_row, expected_result, mock_urllib_request_urlopen):
    file_processor = FileProcessor(url, validate_row, prepare_row)
    result = file_processor.process_file()
    assert result == expected_result


@pytest.mark.parametrize(
    'url, validate_row, prepare_row',
    [(football_url, football_validate_row, football_prepare_row),
     (weather_url, weather_validate_row, weather_prepare_row)
     ])
def test_process_file_return_code_internal_server_error(url, validate_row, prepare_row,
                                                        mock_urllib_request_urlopen_code_internal_server_error):
    with pytest.raises(ConnectionError):
        file_processor = FileProcessor(url, validate_row, prepare_row)
        file_processor.process_file()


@pytest.mark.parametrize(
    'url, validate_row, prepare_row',
    [(football_url, football_validate_row, football_prepare_row),
     (weather_url, weather_validate_row, weather_prepare_row)
     ])
def test_process_file__error(url, validate_row, prepare_row, mock_urllib_request_urlopen_error):
    with pytest.raises(AttributeError):
        file_processor = FileProcessor(url, validate_row, prepare_row)
        file_processor.process_file()
